var gulp        = require('gulp'),
    gutil       = require('gulp-util'),
    uglify      = require('gulp-uglify'),
    jade        = require('gulp-jade'),
    livereload  = require('gulp-livereload'), // Livereload plugin needed: https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei
    tinylr      = require('tiny-lr'),
    express     = require('express'),
    app         = express(),
    path        = require('path'),
    stylus      = require('gulp-stylus'),
    minify      = require('gulp-minify-css'),
    concat      = require('gulp-concat-css'),
    nib         = require('nib'),
    imageminJpegtran = require('imagemin-jpegtran'),
    server      = tinylr();

gulp.task('images', function() {
    return gulp.src('./frontdev/img/*.*')
        .pipe(imageminJpegtran({progressive: true})())
        .pipe(gulp.dest('./staticfiles/images/'));
});

gulp.task('fonts', function() {
    return gulp.src('./frontdev/fonts/*.*')
        .pipe(gulp.dest('./staticfiles/fonts/'));
});


gulp.task('express', function() {
  app.use(express.static(path.resolve('./staticfiles')));
  app.listen(3000);
  gutil.log('Listening on port: 3000');
});

gulp.task('jade', function() {
  return gulp.src('./frontdev/index.jade')
    .pipe(jade({
      pretty: false
    }))
    .pipe(gulp.dest('./staticfiles/'))
    .pipe( livereload( server ));
});

gulp.task('styl', function() {
  return gulp.src('./frontdev/stylus/*.styl')
    .pipe( 
      stylus( { 
        use:nib()
      } ) )
    .pipe(concat('app.css'))
    .pipe( minify() )
    .pipe( gulp.dest('./staticfiles/css/') )
    .pipe( livereload( server, {auto: true}));
});

gulp.task('watch', function () {
  server.listen(35728, function (err) {
    if (err) {
      return console.log(err);
    }

    gulp.watch('./frontdev/stylus/*.styl',['styl']);
    gulp.watch('./staticfiles/css/app.css');
    
  });
});


gulp.task('default', ['express','jade','styl','images','fonts','watch']);