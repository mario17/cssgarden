
# CSSGARDEN

Please follow the instructions before run the project.

## Setting up the Env.

Install node and bower requirements. We need to have gulp, bower, sass and compass clients.
```bash
$ npm install
$ bower install
```

## Generate static files

Just run:

```bash
$ gulp
$ bower
```

To supervice any change and autogenerate.

```bash
$ gulp watch
```

Good luck...